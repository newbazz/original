var feedbackRenderer = function() {
    var cntDiv = getContentDiv();
    var buildFeedback = function() {
      document.getElementsByClassName('content')[0].innerHTML = '';
      var iframe = createIframe('./js/feedback.html');
      var feedbackContainer = createHtmlElementWithId('div', 'feedback-container');
      feedbackContainer.appendChild(iframe);
      document.getElementsByClassName('content')[0].appendChild(feedbackContainer);
    };
    var exp_el = document.getElementsByClassName("feedback-wrapper")[0];
    styleActiveTocEl(exp_el);
    buildFeedback();  
    cntDiv.appendChild(createPrevEl());
    cntDiv.appendChild(createNextEl());
  
};
