var config = {
  "js" :  {
            "jquery"  :  "libs/jquery-1.12.0.min.js",
            "bootstrap" : "libs/bootstrap-3.3.7/dist/js/bootstrap.min.js",
            "apis"  :  "js/exp-dom.js",
            "artefacts": "js/artefacts-path.js",
            "realization": "https://pensive-almeida-613c7e.netlify.com/build/code/runtime/js/realization.js", 
            "layoutRenderer" : "js/layout.js",
            "headerRenderer" : "js/header.js",
            "tocRenderer" : "js/toc.js",
            "contentRenderer" : "js/content.js"
          },

  "css" : {
            "bootstrap" : "libs/bootstrap-3.3.7/dist/css/bootstrap.min.css",
            "fontawesome" : "libs/font-awesome-4.7.0/css/font-awesome.css",
            "common"    : "common-styles/css/common.css",
            "header"    : "header-styles/css/header.css",
            "toc"       : "toc-styles/css/toc.css",
            "content"   : "content-styles/css/content.css"
          }
};
